package com.hotelmanagement;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.hotelmanagement.model.client.Room;
import com.hotelmanagement.repository.ReservationRepository;
import com.hotelmanagement.repository.RoomRepository;
import com.hotelmanagement.service.RoomService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
classes = HotelManagementApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class RoomIT {

	@Autowired
	private RoomRepository roomRepository;
	
	@Autowired 
	RoomService roomService;

	@Test
	public void findAll() {
		roomRepository.save(createRoom("22"));
		roomRepository.save(createRoom("23"));
		roomRepository.save(createRoom("24"));
		assertEquals(3, roomService.findAllRooms().size());
	}
	
	@Test
	public void copy() {
		Room room1 = createRoom("22");
		Room room2 = new Room(); 
		roomService.deepCopy(room2,room1);
		assertEquals(room1.getNumberOfPersons(), room2.getNumberOfPersons());
		assertEquals(room1.getType(), room2.getType());
		assertEquals(room1.getPrice(), room2.getPrice());
	}
	
	@Test
	public void save() {
		roomRepository.save(createRoom("22"));
		roomRepository.save(createRoom("23"));
		roomRepository.save(createRoom("24"));
		assertEquals(3, roomService.findAllRooms().size());
	}
	
	private Room createRoom(String id) {
		Room room = new Room();
		room.setId(id);
		room.setNumberOfPersons(2);
		room.setPrice(Double.valueOf("222.2"));
		room.setType("SINGLE");
		return room;
	}
}
