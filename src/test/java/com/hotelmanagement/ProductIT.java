package com.hotelmanagement;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.hotelmanagement.model.product.Product;
import com.hotelmanagement.repository.ProductRepository;
import com.hotelmanagement.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
classes = HotelManagementApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class ProductIT {
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	ProductService productService;
	
	@Before
	public void before() {
		productRepo.deleteAll();
	}
	
	@Test
	public void findAllProds() {
		productRepo.save(createProduct("name1"));
		productRepo.save(createProduct("name2"));
		productRepo.save(createProduct("name3"));
		assertEquals(3, productService.getAllProducts().size());
	}
	
	@Test
	public void saveProds() {
		productRepo.save(createProduct("name1"));
		assertEquals(1, productService.getAllProducts().size());
	}
	
	@Test
	public void deleteProds() {
		productRepo.save(createProduct("name1"));
		productRepo.save(createProduct("name2"));
		productRepo.save(createProduct("name3"));
		productService.deleteProduct(productRepo.findByName("name1").getId());
		assertEquals(2, productService.getAllProducts().size());
	}
	
	private Product createProduct(String name) {
		Product prod = new Product();
		prod.setDescription("descr");
		prod.setName(name);
		prod.setQuantity(22);
		return prod;
	}
}
