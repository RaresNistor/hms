package com.hotelmanagement;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.hotelmanagement.model.product.Suplier;
import com.hotelmanagement.repository.SuplierRepository;
import com.hotelmanagement.service.SupplierService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
classes = HotelManagementApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class SupplierIT {

	@Autowired
	private SuplierRepository suplierRepository;
	
	@Autowired
	private SupplierService suplierService;
	
	@After
	public void after() {
		suplierRepository.deleteAll();
	}
	
	@Test
	public void findAll() {
		suplierRepository.save(createSuplier("address1", "name1", "0777777777"));
		suplierRepository.save(createSuplier("address2", "name2", "0777777778"));
		suplierRepository.save(createSuplier("address3", "name3", "0777777779"));
		assertEquals(3,  suplierService.findAllSuppliers().size());
	}
	
	@Test
	public void findByName() {
		suplierRepository.save(createSuplier("address1", "name1", "0777777777"));
		Suplier supp = suplierService.findSuplierByName("name1");
		assertEquals("address1",  supp.getAddres());
		assertEquals("0777777777",  supp.getPhoneNumber());
	}
	
	
	@Test
	public void delete() {
		suplierRepository.save(createSuplier("address1", "name1", "0777777777"));
		suplierRepository.save(createSuplier("address2", "name2", "0777777778"));
		suplierRepository.save(createSuplier("address3", "name3", "0777777779"));
		Suplier supp = suplierService.findSuplierByName("name1");
		suplierService.deleteSupplier(supp.getId());
		assertEquals(2,  suplierService.findAllSuppliers().size());
	}
	
	private Suplier createSuplier(String address, String name, String phone) {
		Suplier supp = new Suplier();
		supp.setAddres(address);
		supp.setName(name);
		supp.setPhoneNumber(phone);
		return supp;
	}
}
