package com.hotelmanagement;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.hotelmanagement.model.client.Reservation;
import com.hotelmanagement.model.client.Room;
import com.hotelmanagement.model.user.Role;
import com.hotelmanagement.model.user.User;
import com.hotelmanagement.repository.ClientRepository;
import com.hotelmanagement.repository.ReservationRepository;
import com.hotelmanagement.repository.RoleRespository;
import com.hotelmanagement.repository.RoomRepository;
import com.hotelmanagement.service.ReservationService;
import com.hotelmanagement.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
classes = HotelManagementApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class ReservationIT {

	@Autowired
	private ReservationRepository reservationRepository;

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private RoomRepository roomRepository;

	@Autowired
	private ReservationService reservationService;

	@Autowired
	private UserService userService;

	@Autowired
	private RoleRespository roleRespository;

	@Before
	public void before() {
		clientRepository.deleteAll();
		roomRepository.deleteAll();
		reservationRepository.deleteAll();
		roleRespository.save(new Role(1, "ROLE_CLIENT"));
		roleRespository.save(new Role(2, "ROLE_ADMIN"));
		roleRespository.save(new Role(3, "ROLE_REC"));
	}
	
	
	@Test
	public void saveResservation() throws ParseException {
		userService.saveUser(createUser(roleRespository.findById(1).get(), "client@gmail.com"), true);
		Reservation res = createReservation("22/12/2020", "25/12/2020");
		reservationService.saveReservation(res, "client@gmail.com");
		assertEquals(1, reservationRepository.findAll().size());
	}

	@Test
	public void findRooms() throws ParseException {
		userService.saveUser(createUser(roleRespository.findById(1).get(), "client@gmail.com"), true);
		roomRepository.save(createRoom("22"));
		Reservation res = new Reservation();
		res.setStartdate(stringToDate("22/12/2020"));
		res.setEndDate(stringToDate("25/12/2020"));
		int nrRooms = reservationService.findReservations(res, "client@gmail.com", "SINGLE", 1).size();
		assertEquals(1, nrRooms);
	}
	
	@Test
	public void findReservationForClient() throws ParseException {
		userService.saveUser(createUser(roleRespository.findById(1).get(), "client@gmail.com"), true);
		Reservation res = createReservation("22/12/2020", "25/12/2020");
		reservationService.saveReservation(res, "client@gmail.com");
		int nrReservations = reservationService.findReservationByClient("client@gmail.com").size();
		assertEquals(1,nrReservations);
	}
	
	@Test
	public void deleteReservation() throws ParseException {
		userService.saveUser(createUser(roleRespository.findById(1).get(), "client@gmail.com"), true);
		Reservation res = createReservation("22/12/2020", "25/12/2020");
		reservationService.saveReservation(res, "client@gmail.com");
		res = clientRepository.findByEmail("client@gmail.com").get().getReservations().get(0);
		assertEquals(true,reservationService.deleteReservation(res.getId()));
	}
	
	@Test
	public void deleteReservationFail() throws ParseException {
		userService.saveUser(createUser(roleRespository.findById(1).get(), "client@gmail.com"), true);
		Reservation res = createReservation("22/12/2016", "25/12/2020");
		reservationService.saveReservation(res, "client@gmail.com");
		res = clientRepository.findByEmail("client@gmail.com").get().getReservations().get(0);
		assertEquals(false,reservationService.deleteReservation(res.getId()));
	}
	
	private User createUser(Role role, String emial) {
		User user = new User();
		user.setEmail(emial);
		user.setActive(1);
		user.setFirstname("fn");
		user.setLastname("ln");
		user.setPassword("pass");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		user.setRoles(roles);
		return user;
	}

	private Reservation createReservation(String startDate, String endDate) throws ParseException {
		Reservation res = new Reservation();
		res.setStartdate(stringToDate(startDate));
		res.setEndDate(stringToDate(endDate));
		res.setPrice(Double.valueOf("4444"));
		Room room = createRoom("22");
		roomRepository.save(room);
		res.setRoom(roomRepository.findById("22").get());
		return res;
	}

	private Date stringToDate(String date) throws ParseException {
		return new SimpleDateFormat("dd/MM/yyyy").parse(date);
	}
	
	private Room createRoom(String id) {
		Room room = new Room();
		room.setId(id);
		room.setNumberOfPersons(2);
		room.setPrice(Double.valueOf("222.2"));
		room.setType("SINGLE");
		return room;
	}
}
