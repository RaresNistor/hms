package com.hotelmanagement;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.hotelmanagement.model.client.Reservation;
import com.hotelmanagement.model.client.Room;
import com.hotelmanagement.model.user.Role;
import com.hotelmanagement.model.user.User;
import com.hotelmanagement.repository.ClientRepository;
import com.hotelmanagement.repository.ReservationRepository;
import com.hotelmanagement.repository.RoleRespository;
import com.hotelmanagement.repository.RoomRepository;
import com.hotelmanagement.service.GraphService;
import com.hotelmanagement.service.ReservationService;
import com.hotelmanagement.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
classes = HotelManagementApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class GraphIT {

	@Autowired
	private ReservationRepository reservationRepository;

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private RoomRepository roomRepository;

	@Autowired
	private ReservationService reservationService;

	@Autowired
	private UserService userService;

	@Autowired
	private RoleRespository roleRespository;
	
	@Autowired
	GraphService graphService;

	@Before
	public void before() {
		reservationRepository.deleteAll();
		clientRepository.deleteAll();
		roleRespository.save(new Role(1, "ROLE_CLIENT"));
		roleRespository.save(new Role(2, "ROLE_ADMIN"));
		roleRespository.save(new Role(3, "ROLE_REC"));
	}
	
	@Test
	public void getPieChart() throws ParseException {
		userService.saveUser(createUser(roleRespository.findById(1).get(), "client@gmail.com"), true);
		Reservation res = createReservation("22/12/2020", "25/12/2020");
		reservationService.saveReservation(res, "client@gmail.com");
		Reservation res2 = createReservation("22/12/2021", "25/12/2022");
		reservationService.saveReservation(res2, "client@gmail.com");
		Reservation res3 = createReservation("22/12/2022", "25/12/2022");
		reservationService.saveReservation(res3, "client@gmail.com");
		Reservation res4 = createReservation("22/12/2023", "25/12/2023");
		reservationService.saveReservation(res4, "client@gmail.com");
		List<Integer> result = graphService.getPiechart();
		assertTrue(result.get(0).equals(4));
		assertTrue(result.get(1).equals(0));
		assertTrue(result.get(2).equals(0));
	}
	
	@Test
	public void getbarGraph() throws ParseException {
		userService.saveUser(createUser(roleRespository.findById(1).get(), "client@gmail.com"), true);
		Reservation res = createReservation("22/12/2020", "25/12/2020");
		reservationService.saveReservation(res, "client@gmail.com");
		Reservation res2 = createReservation("22/12/2021", "25/12/2022");
		reservationService.saveReservation(res2, "client@gmail.com");
		Reservation res3 = createReservation("22/12/2022", "25/12/2022");
		reservationService.saveReservation(res3, "client@gmail.com");
		Reservation res4 = createReservation("22/12/2023", "25/12/2023");
		reservationService.saveReservation(res4, "client@gmail.com");
		Map<Integer, Integer> map =graphService.getBarGraph();
		assertTrue(map.containsKey(2020));
		assertTrue(map.containsKey(2021));
		assertTrue(map.containsKey(2022));
		assertTrue(map.containsKey(2023));
	}
	
	private Reservation createReservation(String startDate, String endDate) throws ParseException {
		Reservation res = new Reservation();
		res.setStartdate(stringToDate(startDate));
		res.setEndDate(stringToDate(endDate));
		res.setPrice(Double.valueOf("4444"));
		Room room = createRoom("22");
		roomRepository.save(room);
		res.setRoom(roomRepository.findById("22").get());
		return res;
	}

	private Date stringToDate(String date) throws ParseException {
		return new SimpleDateFormat("dd/MM/yyyy").parse(date);
	}
	
	private Room createRoom(String id) {
		Room room = new Room();
		room.setId(id);
		room.setNumberOfPersons(2);
		room.setPrice(Double.valueOf("222.2"));
		room.setType("SINGLE");
		return room;
	}
	
	private User createUser(Role role, String emial) {
		User user = new User();
		user.setEmail(emial);
		user.setActive(1);
		user.setFirstname("fn");
		user.setLastname("ln");
		user.setPassword("pass");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		user.setRoles(roles);
		return user;
	}
}
