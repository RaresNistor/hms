package com.hotelmanagement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.hotelmanagement.model.client.Client;
import com.hotelmanagement.model.user.Role;
import com.hotelmanagement.model.user.User;
import com.hotelmanagement.repository.ClientRepository;
import com.hotelmanagement.repository.RoleRespository;
import com.hotelmanagement.repository.UserRepository;
import com.hotelmanagement.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
classes = HotelManagementApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class UserIT {

	@LocalServerPort
	private int port;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private RoleRespository roleRespository;

	@Before
	public void before() {
		userRepository.deleteAll();
		clientRepository.deleteAll();
		roleRespository.save(new Role(1, "ROLE_CLIENT"));
		roleRespository.save(new Role(2, "ROLE_ADMIN"));
		roleRespository.save(new Role(3, "ROLE_REC"));
	}

	@After()
	public void after(){
		userRepository.deleteAll();
		clientRepository.deleteAll();
	}

	@Test
	public void createUserAndClient() {
		User user = null;
		userService.saveUser(createUser(roleRespository.findById(1).get(), "client@gmail.com"), true);

		user = userRepository.findByEmail("client@gmail.com");
		assertTrue(user != null);
		Client client = clientRepository.findByEmail("client@gmail.com").get();
		assertTrue(client != null);
	}

	@Test
	public void createUser() {
		User user = null;
		userService.saveUser(createUser(roleRespository.findById(3).get(),"user@gmail.com"), false);

		user = userRepository.findByEmail("user@gmail.com");
		assertTrue(user != null);
		assertTrue(!clientRepository.findByEmail("user@gmail.com").isPresent());
		userRepository.deleteAll();
		clientRepository.deleteAll();
		roleRespository.deleteAll();
	}

	@Test
	public void updateUser() {
		userService.saveUser(createUser(roleRespository.findById(3).get(),"user@gmail.com"), false);
		User u = userRepository.findByEmail("user@gmail.com");
		u.setFirstname("Gigi");
		userService.updateUser(u);
		assertEquals("Gigi", userRepository.findByEmail("user@gmail.com").getFirstname());
	}


	@Test
	public void findAll() {
		userService.saveUser(createUser(roleRespository.findById(3).get(),"user1@gmail.com"), false);
		userService.saveUser(createUser(roleRespository.findById(3).get(),"user2@gmail.com"), false);
		userService.saveUser(createUser(roleRespository.findById(3).get(),"user3@gmail.com"), false);
		assertEquals(3,userService.findAllUsers().size());
	}

	@Test
	public void delete() {
		userService.saveUser(createUser(roleRespository.findById(3).get(),"user1@gmail.com"), false);
		userService.saveUser(createUser(roleRespository.findById(3).get(),"user2@gmail.com"), false);
		userService.saveUser(createUser(roleRespository.findById(3).get(),"user3@gmail.com"), false);
		userService.deleteUser(userService.findUserByEmail("user1@gmail.com").getId());
		assertEquals(2,userService.findAllUsers().size());
	}

	private User createUser(Role role, String emial) {
		User user = new User();
		user.setEmail(emial);
		user.setActive(1);
		user.setFirstname("fn");
		user.setLastname("ln");
		user.setPassword("pass");
		List<Role> roles = new ArrayList<>();
		roles.add(role);
		user.setRoles(roles);
		return user;
	}
}
