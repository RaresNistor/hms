package com.hotelmanagement.configuration;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.braintreegateway.BraintreeGateway;

@Configuration
public class BraintreeConfig {
    
    private static String CONFIG_FILENAME = "config.properties";

    @Bean
    public BraintreeGateway getBraintreeGateway() {
        Map<String,String> configMap = getMap();        
        
        BraintreeGateway gateway = null;
        
            try {
            gateway = BraintreeGatewayFactory.fromConfigMapping(configMap);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        return gateway;
    }
    
    
    private Map<String,String> getMap() {
        Map<String,String> map = new HashMap<String,String>();
        
        try {
            Resource resource = new ClassPathResource(CONFIG_FILENAME);
            InputStream is = resource.getInputStream();
            
            Properties properties = new Properties();
            properties.load(is);
            
            map.putAll(properties.entrySet().stream()
                    .collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue().toString())));            
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        return map;
    }
}
