package com.hotelmanagement.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hotelmanagement.model.dto.Search;
import com.hotelmanagement.model.product.Product;
import com.hotelmanagement.model.product.Suplier;
import com.hotelmanagement.service.ProductService;
import com.hotelmanagement.service.SupplierService;

@Controller
@RolesAllowed("ROLE_ADMIN")
public class SupplierController {

	@Autowired
	SupplierService suplierService;

	@Autowired
	ProductService productService;

	public SupplierController() {
		super();
	}

	/**
	 * Prepares the supplier home page
	 * @return the model used to populate the page
	 */
	@GetMapping("/supplier/home")
	public ModelAndView home() {
		ModelAndView model = new ModelAndView();
		model.addObject("suppliers", suplierService.findAllSuppliers());
		model.setViewName("supplier/home");
		model.addObject("search", new Search());
		return model;
	}

	/**
	 * Prepares the create supplier page
	 * @return the model used to populate the page
	 */
	@GetMapping("/supplier/create")
	public ModelAndView create(Suplier supplier) {
		ModelAndView model = new ModelAndView();
		Product p = new Product();
		model.addObject("product", p);
		model.addObject("supplier", supplier);
		model.setViewName("supplier/edit");
		model.addObject("hideCreate", 1);
		return model;
	}

	/**
	 * Prepares the supplier edit page
	 * @param nr the supplier id
	 * @return the model used to populate the page
	 */
	@GetMapping("/supplier/edit/{nr}")
	public ModelAndView edit(@PathVariable String nr) {
		ModelAndView model = new ModelAndView();
		Product p = new Product();
		Suplier supplier = suplierService.findSuplierByName(nr);
		model.addObject("supplier", supplier);
		model.addObject("product", p);
		model.setViewName("supplier/edit");
		model.addObject("hideCreate", 0);
		return model;
	}

	/**
	 * Save the supplier with the new information
	 * @param supplier the supplier to be saved
	 * @return the model used to populate the page
	 */
	@RequestMapping(value = { "/supplier/edit" }, params = "save")
	public ModelAndView createSupplier(Suplier supplier) {
		if(suplierService.save(supplier)) {
			return prepareHomePage("Supplier was created");
		}else {
			return prepareHomePage("Another supplier with the inserted data exists");
		}
	}

	/**
	 * Save the supplier with edited new information
	 * @param supplier the supplier to be edited
	 * @return the model used to populate the page
	 */
	@RequestMapping(value = { "/supplier/edit" }, params = "edit")
	public ModelAndView update(Suplier supplier) {
		if(suplierService.update(supplier)) {
			return prepareHomePage("Supplier data was updted");
		}else {
			return prepareHomePage("Another supplier with the modified data exists");
		}
	}

	/**
	 * Deletes the selected supplier
	 * @param supplier the supplier to be deleted
	 * @return the model used to populate the page
	 */
	@RequestMapping(value = { "/supplier/edit" }, params = "delete")
	public ModelAndView delete(Suplier supplier) {
		suplierService.deleteSupplier(suplierService.findSuplierByName(supplier.getName()).getId());
		return prepareHomePage("Supplier was deleted");
	}

	@GetMapping("/supplier/product")
	@ResponseBody
	public String delete(String id) {
		return id;
	}

	@GetMapping("/supplier/product/add")
	@ResponseBody
	public String addProduct(String id) {
		return id;
	}
	
	/**
	 * Adds a new product to the supplier
	 * @param product the product to be added
	 * @return the model used to populate the page
	 */
	@RequestMapping(value = { "/supplier/product/addProd" })
	public ModelAndView addProduct(Product product) {
		Suplier supplier = suplierService.findSupplierByID(product.getId());
		product.setId(null);
		supplier.getProducts().add(product);
		product.setSuplier(supplier);
		productService.saveProduct(product);
		return edit(supplier.getName());
	}

	/**
	 * Deletes a product from the supplier
	 * @param id the id of the product to be deleted
	 * @return the model used to populate the page
	 */
	@PostMapping("/supplier/product/delete")
	@ResponseBody
	public ModelAndView deleteProduct(long id) {
		Product prod = productService.getProduct(id);
		productService.deleteProduct(id);
		return edit(prod.getSuplier().getName());
	}

	private ModelAndView prepareHomePage(String msg) {
		ModelAndView model = new ModelAndView();
		model.addObject("suppliers", suplierService.findAllSuppliers());
		model.setViewName("supplier/home");
		model.addObject("msg", msg);
		model.addObject("search", new Search());
		model.addObject("currentPage", 0);
		return model;
	}

}
