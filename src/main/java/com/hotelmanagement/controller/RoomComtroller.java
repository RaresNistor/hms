package com.hotelmanagement.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hotelmanagement.model.client.Room;
import com.hotelmanagement.model.dto.RoomProduct;
import com.hotelmanagement.model.dto.Search;
import com.hotelmanagement.model.product.Product;
import com.hotelmanagement.service.ProductService;
import com.hotelmanagement.service.RoomService;

@Controller
@RolesAllowed("ROLE_ADMIN")
public class RoomComtroller {

	@Autowired
	RoomService roomService;

	@Autowired
	ProductService productService;

	public RoomComtroller() {
		super();
	}

	/**
	 * Prepares the rooms page
	 * @return the model used to populate the page
	 */
	@GetMapping("/room/home")
	public ModelAndView home() {
		ModelAndView model = new ModelAndView();
		model.addObject("rooms", roomService.findAllRooms());
		model.addObject("search", new Search());
		model.setViewName("room/home");
		return model;
	}

	/**
	 * Prepares the create/edit room page
	 * @param room the room to be edited
	 * @return the model used to populate the page
	 */
	@GetMapping("/room/create")
	public ModelAndView createRoom(Room room) {
		ModelAndView model = new ModelAndView();
		model.addObject("room", room);
		model.addObject("roomProduct", new RoomProduct());
		model.addObject("types", RoomService.types);
		model.addObject("editName", 1);
		model.addObject("hideCreate", 1);
		model.addObject("products", productService.getAllProducts());
		model.setViewName("room/edit");
		return model;
	}

	/**
	 * Prepares the room to be edited
	 * @param nr the room number
	 * @return the model used to populate the page
	 */
	@GetMapping("/room/edit/{nr}")
	public ModelAndView edit(@PathVariable String nr) {
		ModelAndView model = new ModelAndView();
		Room room = roomService.findRoomById(nr);
		model.addObject("room", room);
		model.addObject("roomProduct", new RoomProduct());
		model.addObject("types", RoomService.types);
		model.addObject("editName", 0);
		model.addObject("products", productService.getAllProducts());
		model.setViewName("room/edit");
		model.addObject("hideCreate", 0);
		return model;
	}

	/**
	 * Creates a room with the inserted data 
	 * @param room the room to be saved
	 * @return the model used to populate the page
	 */
	@RequestMapping(value= {"/room/edit"}, params = "save")
	public ModelAndView create(Room room) {
		if(roomService.findRoomById(room.getId()) == null) {
			roomService.saveRoom(room);
			return  prepareHomePage("Room waas saved");
		}else {
			return  prepareHomePage("Room with that name already exists");
		}
	}

	/**
	 * Edits the room with the inserted data
	 * @param room the room to be edited
	 * @return the model used to populate the page
	 */
	@RequestMapping(value= {"/room/edit"}, params = "edit")
	public ModelAndView update(Room room) {
		Room oldRoom = roomService.findRoomById(room.getId());
		roomService.deepCopy(oldRoom, room);
		roomService.saveRoom(oldRoom);
		return  prepareHomePage("Room was modified");
	}

	/**
	 * Deletes the selected room
	 * @param room the room to be deleted
	 * @return the model used to populate the page
	 */
	@RequestMapping(value= {"/room/edit"}, params = "delete")
	public ModelAndView delete(Room room) {
		roomService.deleteRoom(room.getId());
		return prepareHomePage("Room was deleted");
	}

	/**
	 * Displayes the pop up to delete a product from a room
	 * @param roomId the room id
	 * @param productId the product id 
	 * @return the model used to populate the page
	 */
	@GetMapping("/room/deleteProduct")
	@ResponseBody
	public RoomProduct deleteProduct( String roomId, Long productId) {
		RoomProduct roomProduct = new RoomProduct();
		roomProduct.setRoomId(roomId);
		roomProduct.setProductId(productId);
		return roomProduct;
	}

	/**
	 * Deletes the product from the room
	 * @param roomProduct dto to transfer data 
	 * @return the model used to populate the page
	 */
	@PostMapping("/room/deleteProd")
	@ResponseBody
	public ModelAndView deleteProduct(RoomProduct roomProduct)
	{
		Room room = roomService.findRoomById(roomProduct.getRoomId());
		Product product = productService.getProduct(roomProduct.getProductId());
		product.setQuantity(product.getQuantity()+1);
		productService.saveProduct(product);
		room.getProducts().removeIf(obj -> obj.getId().equals(roomProduct.getProductId()));
		roomService.saveRoom(room);
		return edit(room.getId());
	}

	/**
	 * Prepares the pop up to add a product to the room
	 * @param roomId the room id 
	 * @return the model used to populate the page
	 */
	@GetMapping("/room/addProduct")
	@ResponseBody
	public RoomProduct addProduct( String roomId)
	{
		RoomProduct roomProduct = new RoomProduct();
		roomProduct.setRoomId(roomId);
		roomProduct.setProducts(productService.getAllProds());
		return roomProduct;
	}


	/**
	 * Add a product to the room
	 * @param roomProduct the dto used to transfer data 
	 * @return the model used to populate the page
	 */
	@PostMapping("/room/addProduct")
	@ResponseBody
	public ModelAndView addProduct(RoomProduct roomProduct)
	{
		Room room = roomService.findRoomById(roomProduct.getRoomId());
		Product product = productService.getProduct(roomProduct.getProductId());
		product.setQuantity(product.getQuantity()-1);
		room.getProducts().add(product);
		roomService.saveRoom(room);
		return edit(room.getId());
	}

	private ModelAndView prepareHomePage(String message) {
		ModelAndView model = new ModelAndView();
		model.addObject("rooms", roomService.findAllRooms());
		model.setViewName("room/home");
		model.addObject("search", new Search());
		model.addObject("currentPage", 0);
		model.addObject("msg", message);
		return model;
	}
}
