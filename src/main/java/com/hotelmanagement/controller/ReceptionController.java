package com.hotelmanagement.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hotelmanagement.model.user.User;
import com.hotelmanagement.service.ReservationService;
import com.hotelmanagement.service.UserService;

@Controller
public class ReceptionController {

	@Autowired
	private UserService userService;

	@Autowired
	ReservationService reservationService;
	
	/**
	 * Prepares the receptionist home page 
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_REC")
	@GetMapping("/reception/home")
	public ModelAndView home() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		ModelAndView model = new ModelAndView();
		model.addObject("user", user);
		model.setViewName("reception/home");
		return model;
	}
	
	/**
	 * Prepares the reservation page with the needed data
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_REC")
	@GetMapping("/reception/reservations")
	public ModelAndView reservationsHome() {
		ModelAndView model = new ModelAndView();
		model.addObject("reservations", reservationService.findAll());
		model.setViewName("reception/reservations");
		return model;
	}
	
	/**
	 * Deletes the selected reservation
	 * @param id the reservation to be deleted
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_REC")
	@GetMapping("/reception/reservation/delete")
	@ResponseBody
	public ModelAndView delete( long id) {
		ModelAndView model = new ModelAndView();
		if(reservationService.deleteReservation(id)) {
			model.addObject("msgSuccess", "Reservation was deleted!");
		}else {
			  model.addObject("msgFail", "Cannot delete reservation!");
		}
		
		model.addObject("reservations", reservationService.findAll());
		model.setViewName("/reception/reservations");
		return model;
	}
	
}
