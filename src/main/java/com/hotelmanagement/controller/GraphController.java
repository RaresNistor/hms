package com.hotelmanagement.controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hotelmanagement.service.GraphService;

@RolesAllowed("ROLE_REC")
@Controller
public class GraphController {

	@Autowired
	GraphService graphService;
	
	@GetMapping("/displayBarGraph")
	public String barGraph(Model model) {
		model.addAttribute("surveyMap", graphService.getBarGraph());
		return "manager/barGraph";
	}

	/**
	 * Prepares the pie chart page
	 * @param model the model used to populate the page
	 * @return the model used to populate the page
	 */
	@GetMapping("/displayPieChart")
	public String pieChart(Model model) {
		List<Integer> results = graphService.getPiechart();
		model.addAttribute("single", results.get(0));
		model.addAttribute("doublee", results.get(1));
		model.addAttribute("triple", results.get(2));
		return "manager/pieChart";
	}
	
	/**
	 * Prepares the graph page 
	 * @return the model used to populate the page
	 */
	@GetMapping("/graph")
	public ModelAndView showInfoAdmin() {
		  ModelAndView model = new ModelAndView();
		  model.setViewName("manager/home");
		  return model;
	 }

}