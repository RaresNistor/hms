package com.hotelmanagement.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hotelmanagement.model.HotelInfo;
import com.hotelmanagement.service.HotelInfoService;

@Controller
public class HotelInfoController {
	private static final String HOTE_INFO = "hotelInfo";
	
	private static final String MSG = "msg";
	
	@Autowired
	HotelInfoService hotelInfoService;
	
	/**
	 * Prepares the hotel info page for user with no rights to modify info
	 * @return the model used to populate the page
	 */
	@RolesAllowed({"ROLE_REC", "ROLE_CLIENT"})
	@GetMapping("/client/hotelInfo")
	public ModelAndView showInfoClient() {
		  ModelAndView model = new ModelAndView();
		  model.setViewName("client/hotelInfo");
		  HotelInfo hotelInfo = hotelInfoService.getHotelInfo();
		  if(hotelInfo == null) {
			  model.addObject(HOTE_INFO, new HotelInfo());
			  model.addObject(MSG, "Could not load data.");
		  }else {
			  model.addObject(HOTE_INFO, hotelInfoService.getHotelInfo());
		  }
		  return model;
	 }
	
	/**
	 * Prepares the hotel info page for the admin
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_ADMIN")
	@GetMapping("/admin/hotelInfo")
	public ModelAndView showInfoAdmin() {
		  ModelAndView model = new ModelAndView();
		  model.setViewName("user/hotelInfo");
		  HotelInfo hotelInfo = hotelInfoService.getHotelInfo();
		  if(hotelInfo == null) {
			  model.addObject(MSG, "Could not load data.");
			  model.addObject(HOTE_INFO, new HotelInfo());
		  }else {
			  model.addObject(HOTE_INFO, hotelInfoService.getHotelInfo());
		  }
		  return model;
	 }
	
	/**
	 * Saves the changes to the hotel info
	 * @param hotelInfo the information that will be saved
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_ADMIN")
	@PostMapping("/admin/hotelInfo")
	public ModelAndView info( HotelInfo hotelInfo) {
		  ModelAndView model = new ModelAndView();
		  hotelInfoService.setHotelInfo(hotelInfo);
		  model.addObject(HOTE_INFO, hotelInfo);
		  model.addObject(MSG, "Data saved successfully.");
		  model.setViewName("user/hotelInfo");
		  return model;
	 }
}
