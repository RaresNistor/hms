package com.hotelmanagement.controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hotelmanagement.model.client.Client;
import com.hotelmanagement.model.dto.Search;
import com.hotelmanagement.model.user.User;
import com.hotelmanagement.service.ClientService;
import com.hotelmanagement.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private ClientService clientService;

	private static final String SIGNUP = "user/signup";

	@Autowired
	public UserController() {
		super();
	}

	/**
	 * Redirect to the login page
	 * @return the model used to populate the page
	 */
	@RequestMapping(value = { "/", "/login" })
	public ModelAndView loginGet() {
		ModelAndView model = new ModelAndView();
		model.setViewName("user/login");
		return model;
	}

	/**
	 * Redirect to the sign up page 
	 * @return the model used to populate the page
	 */
	@GetMapping(value = { "/signup" })
	public ModelAndView signup() {
		ModelAndView model = new ModelAndView();
		Client client = new Client();
		model.addObject("client", client);
		model.setViewName(SIGNUP);

		return model;
	}

	/**
	 * Creates a new client
	 * @param client the client to be created
	 * @param bindingResult object to add binding specific analysis
	 * @return the model used to populate the page
	 */
	@PostMapping(value = { "/signup" })
	public ModelAndView createUser(@Valid Client client, BindingResult bindingResult) {
		ModelAndView model = new ModelAndView();
		User userExists = userService.findUserByEmail(client.getEmail());

		if (userExists != null) {
			bindingResult.rejectValue("email", "error.user", "This email already exists!");
		}
		if (bindingResult.hasErrors()) {
			model.setViewName(SIGNUP);
		} else {
			User user = clientService.map(client);
			user.setPassword(client.getPassword());
			userService.saveUser(user, false);
			clientService.saveClient(client);
			model.addObject("msg", "User has been registered successfully!");
			model.addObject("user", new Client());
			model.setViewName("user/login");
		}
		return model;
	}

	/**
	 * Redirects to the admin home page
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_ADMIN")
	@GetMapping("/admin/home")
	public ModelAndView home() {
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		model.addObject("userName", user);
		model.setViewName("admin/home");
		return model;
	}

	/**
	 * Prepares the view users page
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_ADMIN")
	@GetMapping("/user/home")
	public ModelAndView userHome() {
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		model.addObject("userName", user);
		List<User> users = userService.findAllUsers();
		model.addObject("users", users);
		model.addObject("search", new Search());
		model.setViewName("user/home");
		return model;
	}

	/**
	 * Redirecrs to the access denied page
	 * @return the model used to populate the page
	 */
	@GetMapping("/access_denied")
	public ModelAndView accessDenied() {
		ModelAndView model = new ModelAndView();
		model.setViewName("errors/access_denied");
		return model;
	}

	/**
	 * Prepares the create user page
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_ADMIN")
	@GetMapping("/create")
	public ModelAndView create() {
		ModelAndView model = new ModelAndView();
		User user = new User();
		user.setPassword(null);
		user.setEmail(null);
		user.setRrole("CLIENT");
		model.addObject("user", user);
		model.addObject("allRoles", userService.getAllRoles());
		model.setViewName("user/edit");
		model.addObject("hideCreate", 1);
		return model;
	}

	/**
	 * Prepares the edit user page
	 * @param email the email of the user to be edited
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_ADMIN")
	@GetMapping("/edit/{email}")
	public ModelAndView edit(@PathVariable String email) {
		ModelAndView model = new ModelAndView();
		User user = userService.findUserByEmail(email);
		user.setRrole(user.getRoles().get(0).getRole());
		model.addObject("user", user);
		model.addObject("allRoles", userService.getAllRoles());
		model.addObject("hideCreate", 0);
		model.setViewName("user/edit");
		return model;
	}

	/**
	 * Deletes the selected user
	 * @param user the user to be deleted
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_ADMIN")
	@RequestMapping(value = "/edit", params = "delete")
	public ModelAndView delete(User user) {
		userService.deleteUser(userService.findUserByEmail(user.getEmail()).getId());
		return prepareHomePage("User was deleted");
	}

	/**
	 * Edits the selected user
	 * @param user the user to be edited 
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_ADMIN")
	@RequestMapping(value = "/edit", params = "edit")
	public ModelAndView update(@Valid User user) {
		User oldUser = userService.findUserById(user.getId());
		User checkUser = userService.findUserByEmail(user.getEmail());
		if (checkUser != null && !checkUser.getEmail().equals(oldUser.getEmail())) {
			return prepareHomePage("Email already used");
		} else {
			userService.deepCopy(oldUser, user);
			if (!user.getPassword().trim().isEmpty()) {
				oldUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			}
			userService.updateUser(oldUser);
		}
		return prepareHomePage("User was edited");
	}

	/**
	 * Creates a new user
	 * @param user user to be created
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_ADMIN")
	@RequestMapping(value = "/edit", params = "save")
	public ModelAndView save(@Valid User user) {
		userService.deepCopy(user, user);
		if (userService.findUserByEmail(user.getEmail()) != null) {
			return prepareHomePage("User already exists");
		} else {
			userService.saveUser(user, false);
			return prepareHomePage("User was created");
		}
	}

	/**
	 * Prepares the user personal page
	 * @return the model used to populate the page
	 */
	@RolesAllowed({ "ROLE_ADMIN", "ROLE_REC" })
	@GetMapping("/user/myProfile")
	public ModelAndView viewMyProfile() {
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		model.addObject("user", user);
		model.setViewName("user/myProfile");
		return model;
	}

	/**
	 * Save the changes for the user
	 * @param user the user to be edited
	 * @return the model used to populate the page
	 */
	@RolesAllowed({ "ROLE_ADMIN", "ROLE_REC" })
	@PostMapping("/user/myProfile")
	public ModelAndView saveMyProfile(User user) {
		ModelAndView model = new ModelAndView();
		User oldUser = userService.findUserByEmail(user.getEmail());
		oldUser.setFirstname(user.getFirstname());
		oldUser.setLastname(user.getLastname());
		oldUser.setEmail(user.getEmail());
		if (!user.getPassword().trim().isEmpty()) {
			oldUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		}
		userService.saveUser(oldUser);
		model.addObject("user", user);
		if (oldUser.getRoles().get(0).getRole().equals("ROLE_REC")) {
			model.setViewName("reception/home");
		} else {
			model.addObject("userName", oldUser);
			model.setViewName("admin/home");
		}
		return model;
	}

	private ModelAndView prepareHomePage(String message) {
		ModelAndView model = new ModelAndView();
		model.addObject("users", userService.findAllUsers());
		model.setViewName("/user/home");
		model.addObject("search", new Search());
		model.addObject("msg", message);
		return model;
	}

}