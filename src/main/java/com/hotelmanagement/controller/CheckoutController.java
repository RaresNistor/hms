package com.hotelmanagement.controller;


import java.math.BigDecimal;
import java.util.Arrays;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.CreditCard;
import com.braintreegateway.Customer;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.Transaction.Status;
import com.hotelmanagement.model.client.Reservation;
import com.hotelmanagement.service.ReservationService;
import com.braintreegateway.TransactionRequest;
import com.braintreegateway.ValidationError;

@Controller
@RolesAllowed("ROLE_CLIENT")
public class CheckoutController {

    @Autowired
    private BraintreeGateway gateway;
    
    @Autowired
    private ReservationService reservationService;

    private Status[] TRANSACTION_SUCCESS_STATUSES = new Status[] {
        Transaction.Status.AUTHORIZED,
        Transaction.Status.AUTHORIZING,
        Transaction.Status.SETTLED,
        Transaction.Status.SETTLEMENT_CONFIRMED,
        Transaction.Status.SETTLEMENT_PENDING,
        Transaction.Status.SETTLING,
        Transaction.Status.SUBMITTED_FOR_SETTLEMENT
    };
    
    /**
     * Prepares the transaction to be processed
     * @param amount the amount of money to be paid
     * @param id the id of transaction
     * @param nonce the type of transaction
     * @param model the view model
     * @param redirectAttributes A specialization of the Model interface that controllers can use to select attributes for a redirect scenario
     * @return the url of the next page
     */
    @PostMapping(value = "/checkouts")
    public String postForm(@RequestParam("amount") String amount, @RequestParam("resId") String id,
            @RequestParam("payment_method_nonce") String nonce, Model model, 
            final RedirectAttributes redirectAttributes) {
        
        amount = amount.trim();
        BigDecimal decimalAmount;
        long reservationId;
        try {
            decimalAmount = new BigDecimal(amount);
            reservationId = Long.parseLong(id);
        } catch (NumberFormatException e) {
            String errorMessage = getErrorMessage(amount);
            redirectAttributes.addFlashAttribute("errorDetails", errorMessage);
            redirectAttributes.addFlashAttribute("amount", amount);
            return "redirect:checkouts";
        }
        
        Reservation reservation = reservationService.findReservationById(reservationId);
        reservation.setIsPayed(true);
        reservationService.saveReservation(reservation);
        
        TransactionRequest request = new TransactionRequest()
            .amount(decimalAmount)
            .paymentMethodNonce(nonce)
            .options()
                .submitForSettlement(true)
                .done();

        Result<Transaction> result = gateway.transaction().sale(request);
        
        if (result.isSuccess()) {
            Transaction transaction = result.getTarget();
            return "redirect:checkouts/" + transaction.getId();
        } else if (result.getTransaction() != null) {
            Transaction transaction = result.getTransaction();
            return "redirect:checkouts/" + transaction.getId();
        } else {
            String errorString = "";
            for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
               errorString += "Error: " + error.getCode() + ": " + error.getMessage() + "\n";
            }
            redirectAttributes.addFlashAttribute("errorDetails", errorString);
            redirectAttributes.addFlashAttribute("amount", amount);
            return "redirect:checkouts";
        }
    }

    
    private String getErrorMessage(String amount) {
        String errorMessage = amount + " is not a valid price.";

        if (amount.equals("")) {
            errorMessage = "Please enter a valid price.";
        } 
        
        return errorMessage;
    }
    

    /**
     * Sends the transaction to braintree
     * @param transactionId the id of the transaction
     * @param model the model to be displayed
     * @return the new url
     */
    @RequestMapping(value = "/checkouts/{transactionId}")
    public String getTransaction(@PathVariable String transactionId, Model model) {
        Transaction transaction;
        CreditCard creditCard;
        Customer customer;

        try {
            transaction = gateway.transaction().find(transactionId);
            creditCard = transaction.getCreditCard();
            customer = transaction.getCustomer();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            return "redirect:/checkouts";
        }

        model.addAttribute("isSuccess", Arrays.asList(TRANSACTION_SUCCESS_STATUSES).contains(transaction.getStatus()));
        
        model.addAttribute("transaction", transaction);
        model.addAttribute("creditCard", creditCard);
        model.addAttribute("customer", customer);

        return "transactionResults";
    }
    
    /**
     * Prepares the reservation to be payed
     * @param id the id of the reservation
     * @return the model used to populate the page
     */
	@GetMapping("/reservation/pay")
	@ResponseBody
	public ModelAndView edit(long id) {
		ModelAndView model = new ModelAndView();
		Reservation reservation = reservationService.findReservationById(id);
        String clientToken = gateway.clientToken().generate();
        model.addObject("clientToken", clientToken);
		model.addObject("reservation", reservation);
		model.setViewName("newTransaction");
		return model;
	}
}
