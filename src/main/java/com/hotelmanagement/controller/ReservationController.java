package com.hotelmanagement.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hotelmanagement.model.client.Client;
import com.hotelmanagement.model.client.Reservation;
import com.hotelmanagement.model.client.Room;
import com.hotelmanagement.model.dto.Details;
import com.hotelmanagement.model.dto.DetailsWrapper;
import com.hotelmanagement.model.dto.ReservationDTO;
import com.hotelmanagement.service.ClientService;
import com.hotelmanagement.service.ReservationService;
import com.hotelmanagement.service.RoomService;
import com.hotelmanagement.service.UserService;

@Controller
public class ReservationController {
	private static final String DISPLAY_RESULT = "displayResult";

	private static final String RESERVATION = "reservation";

	private static final String TYPES = "types";

	@Autowired
	ReservationService reservationService;

	@Autowired
	UserService userService;

	@Autowired
	RoomService roomService;

	@Autowired
	ClientService clientService;

	/**
	 * Prepares the search reservation page
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_CLIENT")
	@GetMapping("/client/reservation")
	public ModelAndView home() {
		ModelAndView model = new ModelAndView();
		model.addObject(RESERVATION, new ReservationDTO());
		model.addObject(TYPES, RoomService.types);
		model.addObject(DISPLAY_RESULT, 0);
		model.setViewName("client/reservation");
		return model;
	}

	/**
	 * Creates a reservation for the selected search criteria and saves it
	 * @param res the dto used to create a reservation
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_CLIENT")
	@PostMapping("/client/reservation")
	@ResponseBody
	public ModelAndView search(ReservationDTO res) {
		Reservation reservation = null;
		List<Room> rooms = null;
		try {
			reservation = reservationService.map(res);
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			rooms = reservationService.findReservations(reservation, auth.getName(), res.getRoomType(),
					res.getNumberOfPersons());
		} catch (ParseException e) {
		}
		ModelAndView model = new ModelAndView();
		model.addObject(TYPES, RoomService.types);
		model.addObject(RESERVATION, res);
		model.addObject(DISPLAY_RESULT, 1);
		model.addObject("rooms", rooms);
		model.setViewName("/client/reservation");
		return model;
	}

	/**
	 * Adds additional information to the reservation 
	 * @param nr the room number
	 * @param startDate the start date of the reservation
	 * @param endDate the end date of the reservation
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_CLIENT")
	@GetMapping("/client/reservation/save")
	@ResponseBody
	public ModelAndView save(String nr, String startDate, String endDate) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		Reservation reservation = new Reservation();
		reservation.setRoom(roomService.findRoomById(nr));
		Client client = clientService.findClientByEmail( auth.getName());
		try {
			reservation.setStartdate(new SimpleDateFormat("dd/MM/yyyy").parse(startDate));
			reservation.setEndDate(new SimpleDateFormat("dd/MM/yyyy").parse(endDate));
		} catch (ParseException e) {
		}
		reservation.setPrice(reservation.getRoom().getPrice() * TimeUnit.DAYS.convert(
				reservation.getEndDate().getTime() - reservation.getStartdate().getTime(), TimeUnit.MILLISECONDS));
		reservationService.saveReservation(reservation, auth.getName());
		client.getReservations().add(reservation);
		ModelAndView model = new ModelAndView();
		model.addObject("msg", "Reservation created");
		model.addObject("client",client);
		model.setViewName("/client/reservationHistory");
		return model;
	}

	/**
	 * Displays the room details pop up
	 * @param id the room id
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_CLIENT")
	@GetMapping("/client/reservation/details")
	@ResponseBody
	public DetailsWrapper viewDetails(String id) {
		Room room = roomService.findRoomById(id);
		List<Details> details = new ArrayList<>();
		DetailsWrapper wrapper = new DetailsWrapper();
		room.getProducts().forEach(prod -> details.add(new Details(prod.getName(), prod.getDescription())));
		wrapper.setDetails(details);
		return wrapper;
	}

	/**
	 * Prepares the past reservation page
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_CLIENT")
	@GetMapping("/client/reservation/history")
	@ResponseBody
	public ModelAndView history() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Client client = clientService.findClientByEmail(auth.getName());
		ModelAndView model = new ModelAndView();
		model.addObject("client", client);

		model.setViewName("/client/reservationHistory");
		return model;
	}

	/**
	 * Deletes the selected reservation
	 * @param id the id of the reservation
	 * @return the model used to populate the page
	 */
	@RolesAllowed("ROLE_CLIENT")
	@GetMapping("/client/reservation/delete")
	@ResponseBody
	public ModelAndView delete(long id) {
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Client client = clientService.findClientByEmail(auth.getName());
		if (reservationService.deleteReservation(id)) {
			model.addObject("msg", "Reservation was deleted!");
			client.getReservations().removeIf(x -> x.getId().equals(id));
		} else {
			model.addObject("msg", "Cannot delete reservation!");
		}
		model.addObject("client", client);
		model.setViewName("/client/reservationHistory");
		return model;
	}
}
