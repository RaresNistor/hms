package com.hotelmanagement.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hotelmanagement.model.client.Client;
import com.hotelmanagement.model.user.User;
import com.hotelmanagement.service.ClientService;
import com.hotelmanagement.service.UserService;

@Controller
@RolesAllowed("ROLE_CLIENT")
public class ClientController {

	@Autowired
	private UserService userService;

	@Autowired
	private ClientService clientService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	/**
	 * Prepares the client home page
	 * @return the model used to populate the page
	 */
	@GetMapping("/client/home")
	public ModelAndView home() {
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		model.addObject("userName", user);
		model.setViewName("client/home");
		return model;
	}

	/**
	 * Prepares the client profile page
	 * @return the model used to populate the page
	 */
	@GetMapping("/client/myProfile")
	public ModelAndView edit() {
		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Client client = clientService.findClientByEmail(auth.getName());
		model.addObject("client", client);
		model.setViewName("client/myProfile");
		return model;
	}

	/**
	 * Saves the client
	 * @param client the client to be saved
	 * @return the model used to populate the page
	 */
	@PostMapping("/client/myProfile")
	public ModelAndView saveEdit(Client client) {
		ModelAndView model = new ModelAndView();
		boolean cont = false;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User userName = userService.findUserByEmail(auth.getName());
		Client oldClinet = clientService.findClientByEmail(auth.getName());

		if (client.getEmail().equals(auth.getName())) {
			cont = true;
		} else {
			if (userService.findUserByEmail(client.getEmail()) == null) {
				cont = true;
			}
		}

		if (cont) {
			User user = userService.findUserByEmail(auth.getName());
			clientService.map(user, client);
			clientService.map(oldClinet, client);
			if (!client.getPassword().trim().isEmpty()) {
				oldClinet.setPassword(client.getPassword());
				user.setPassword(bCryptPasswordEncoder.encode(client.getPassword()));
			}
			userService.saveUser(user);
			clientService.saveClient(oldClinet);
			model.addObject("userName", userName);
			if (auth.getName().equals(client.getEmail())) {
				model.setViewName("/client/home");
			} else {
				model.setViewName("/user/login");
				auth.setAuthenticated(false);
			}
		}else {
			return edit();
		}
		return model;
	}
}
