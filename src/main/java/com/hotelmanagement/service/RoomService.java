package com.hotelmanagement.service;

import java.util.ArrayList;
import java.util.List;


import com.hotelmanagement.model.client.Room;

public interface RoomService {
	public static final List<String> types = new ArrayList<String>() {
		private static final long serialVersionUID = 1L;
	{
	    add("SINGLE");
	    add("DOUBLE");
	    add("TRIPLE");
	}};
	
	/**
	 * Saves the information from the new room to the old one
	 * @param oldRoom the room that will be set
	 * @param newRoom the room ffrom which data will be extracted
	 */
	public void deepCopy(Room oldRoom, Room newRoom);
	
	/**
	 * Saves the given room
	 * @param room the room to be saved
	 */
	public void saveRoom(Room room);
	
	/**
	 * Deletes the room with the given id
	 * @param id of the room to be deleted
	 */
	public void deleteRoom(String id);
	
	/**
	 * Finds the room with the given id 
	 * @param id the id of the room
	 * @return the room with the given id
	 */
	public Room findRoomById(String id);
	
	/**
	 * Returns all rooms
	 * @return all romms
	 */
	public List<Room> findAllRooms();
}
