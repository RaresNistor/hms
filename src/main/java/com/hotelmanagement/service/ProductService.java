package com.hotelmanagement.service;

import java.util.List;

import com.hotelmanagement.model.dto.Prod;
import com.hotelmanagement.model.product.Product;

public interface ProductService {
	
	/**
	 * Deletes the product with the given id
	 * @param id the id of the product
	 */
	public void deleteProduct(long id);
	
	/**
	 * Retrieves the product with the given id
	 * @param id the id of the product
	 * @return the product with the given id
	 */
	public Product getProduct(long id);
	
	/**
	 * Saves the given product
	 * @param product the product to be saved
	 */
	public void saveProduct(Product product);
	
	/**
	 * Returns all available products
	 * @return all available products
	 */
	public List<Product> getAllProducts();
	
	/**
	 * Returns all mapped prods
	 * @return all mapped prods
	 */
	public List<Prod> getAllProds();
}
