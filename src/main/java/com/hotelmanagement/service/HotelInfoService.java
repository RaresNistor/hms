package com.hotelmanagement.service;

import com.hotelmanagement.model.HotelInfo;

public interface HotelInfoService {
	
	/**
	 * Returns the hotel info from the file
	 * @return the hotel info
	 */
	public HotelInfo getHotelInfo();
	
	/**
	 * Serializes and saves the hotel info
	 * @param hotelInfo the hotel info to be saved
	 */
	public void setHotelInfo(HotelInfo hotelInfo);
}
