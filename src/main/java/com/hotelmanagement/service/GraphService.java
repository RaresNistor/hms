package com.hotelmanagement.service;

import java.util.List;
import java.util.Map;

public interface GraphService {

	/**
	 * Processes and returns the data needed for the pie chart
	 * @return the data needed for the pie chart
	 */
	public List<Integer> getPiechart();
	
	/**
	 * Processes and returns the data needed for the bar graph
	 * @return the data needed for the bar graph
	 */
	public Map<Integer, Integer> getBarGraph();
}
