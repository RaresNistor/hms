package com.hotelmanagement.service;

import java.util.List;

import com.hotelmanagement.model.user.Role;
import com.hotelmanagement.model.user.User;

public interface UserService {
	
	/**
	 * Finds the user with the given email
	 * @param email the email of the user
	 * @return the user with the given email
	 */
	 public User findUserByEmail(String email);
	 
	 /**
	  * Returns the user with the given id
	  * @param id the id of the user
	  * @return the user with the given id
	  */
	 public User findUserById(long id);
	 
	 /**
	  * Saves the given user
	  * @param user the user to be saved
	  */
	 public void saveUser(User user);
	 
	 /**
	  * Saves the given user and creates a client if the user is a client
	  * @param user the user to be saved
	  * @param isClient true if user is aclient
	  */
	 public void saveUser(User user, boolean isClient);
	 
	 /**
	  * Returns all users
	  * @return all users
	  */
	 public List<User> findAllUsers();
	 
	 /**
	  * Deletes the user with the given id
	  * @param id the id of the user
	  */
	 public void deleteUser(Long id);
	 
	 /**
	  * Updates the given user
	  * @param user the user to be updated
	  */
	 public void updateUser(User user);
	 
	 /**
	  * Returns all roles for a user
	  * @return all roles for a user
	  */
	 public List<Role> getAllRoles();
	 
	 /**
	  * Copys information from one user too another
	  * @param oldUser the user which will be modified
	  * @param newUser the user containing the new data
	  */
	 public void deepCopy(User oldUser, User newUser);
}
