package com.hotelmanagement.service;

import java.text.ParseException;
import java.util.List;

import com.hotelmanagement.model.client.Reservation;
import com.hotelmanagement.model.client.Room;
import com.hotelmanagement.model.dto.ReservationDTO;

public interface ReservationService {
	
	/**
	 * Save the given reservation for the given user id.
	 * @param reservation the reservation to be saved
	 * @param user the id of the user who created the reservation
	 */
	public void saveReservation(Reservation reservation, String user);
	
	/**
	 * Deletes the reservation with the given id
	 * @param id the id of the reservation
	 * @return true if the reservation was deleted
	 */
	public boolean deleteReservation(long id);
	
	/**
	 * Maps a reservation to the reservation dto
	 * @param res  the dto from which the data will be extracted
	 * @return the mapped reservation
	 * @throws ParseException
	 */
	public Reservation map(ReservationDTO res)  throws ParseException;
	
	/**
	 * Finds a list of room after the given criteria
	 * @param res a reservation object to be populated
	 * @param user the id of the user whos is searching
	 * @param roomType the type of the room to be searched
	 * @param nrOfpersons the minimum number of persons that can be in that room
	 * @return the list of rooms that satisfy the criterias.
	 */
	public List<Room> findReservations(Reservation res, String user, String roomType, int nrOfpersons);

	/**
	 * Finds the reservations for the given client id
	 * @param clientId the id of the client
	 * @return the list of reservations that belong to the client
	 */
	public List<Reservation> findReservationByClient(String clientId);
	
	/**
	 * Finds all reservations
	 * @return all reservations
	 */
	public List<Reservation> findAll();
	
	/**
	 * Returns the reservation with the given id
	 * @param id the id of the reservations
	 * @return reservation with the given id
	 */
	public Reservation findReservationById(long id);
	
	/**
	 * Saves the reservation
	 * @param res the reservation to be saved
	 */
	public void saveReservation(Reservation res);
}
