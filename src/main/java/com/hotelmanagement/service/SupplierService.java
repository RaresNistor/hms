package com.hotelmanagement.service;

import java.util.List;


import com.hotelmanagement.model.product.Suplier;


public interface SupplierService {
	
	/**
	 * Returns all suppliers
	 * @return all suppliers
	 */
	public List<Suplier> findAllSuppliers();
	
	/**
	 * Finds the supplier with the given name
	 * @param name the name of the supplier
	 * @return the supplier with the given name
	 */
	public Suplier findSuplierByName(String name);
	
	/**
	 * Saves the given supplier
	 * @param supplier the supplier to be saved
	 * @return true if the supplier was saved
	 */
	public boolean save (Suplier supplier);
	
	/**
	 * Updates the given supplier
	 * @param supplier the supplier to be updated
	 * @return true if the supplier was updated
	 */
	public boolean update (Suplier supplier);
	
	/**
	 * Finds the supplier by its id
	 * @param id the id of the supplier
	 * @return the supplier with the given id
	 */
	public Suplier findSupplierByID(long id);
	
	/**
	 * Deletes the supplier with the given id
	 * @param id the id of the supplier
	 */
	public void deleteSupplier(long id);
}
