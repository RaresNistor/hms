package com.hotelmanagement.service;


import java.util.Optional;

import com.hotelmanagement.model.client.Client;
import com.hotelmanagement.model.user.User;

public interface ClientService {
	
	/**
	 * Finds the client with the given email
	 * @param email the email of the client
	 * @return the client with the given email
	 */
	public Client findClientByEmail(String email);
	
	/**
	 * Saves the given client
	 * @param client the client to be saved
	 */
	public void saveClient(Client client);
	
	/**
	 * Maps a client info into a user
	 * @param client the client to be mapped
	 * @return the user with the mapped info
	 */
	public User map(Client client);
	
	
	/**
	 * Maps user to a client
	 * @param user the user to be mapped
	 * @return the client with the mapped info
	 */
	public Client map(User user); 
	
	/**
	 * Maps one client to another
	 * @param oldClient the client who will be mapped
	 * @param client the client with the info that will be map
	 */
	public void map(Client oldClient,Client client);

	/**
	 * Sets the user with the client data
	 * @param user the user that will be set
	 * @param client the client that will set the user
	 */
	public void map(User user, Client client);
	
	/**
	 * Deletes the given client
	 * @param client the client that will be deleted.
	 */
	public void deleteClient(Client client);
	
	/**
	 * Finds the client by id
	 * @param id the id of the client
	 * @return the client with the given id
	 */
	public Optional<Client> findClientById(long id);
}
