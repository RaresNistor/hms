package com.hotelmanagement.service;

import javax.mail.MessagingException;

import com.hotelmanagement.model.client.Reservation;

public interface MailService {

	/**
	 * Sends a mail with the reservation info to the client
	 * @param res the reservation to be sent via mail
	 * @throws MessagingException
	 */
	public void send(Reservation res) throws MessagingException;
}
