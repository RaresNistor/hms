package com.hotelmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hotelmanagement.model.product.Suplier;
import java.util.List;
import java.util.Optional;

public interface SuplierRepository extends JpaRepository<Suplier, Long>{

	Optional<Suplier>  findByName(String name);
	
	Optional<Suplier> findByAddres(String address);
	
	Optional<Suplier> findByPhoneNumber(String phone);
}
