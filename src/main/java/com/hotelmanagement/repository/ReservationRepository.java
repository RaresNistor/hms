package com.hotelmanagement.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hotelmanagement.model.client.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Long>{

	@Query("select count (r) from Reservation r where (r.startdate < :endDate and  r.endDate > :startDate)  and r.room.id = :roomId")
	public long checkIfRoomAvailable(Date startDate,Date endDate, String roomId);
}
