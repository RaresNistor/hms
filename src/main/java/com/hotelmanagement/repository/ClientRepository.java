package com.hotelmanagement.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hotelmanagement.model.client.Client;

@Repository("clientRepository")
public interface ClientRepository extends JpaRepository<Client, Long>{

	public Optional<Client> findByEmail(String email);

	public void deleteByEmail(String email);
	
	public Optional<Client> findById(Long id);
}
