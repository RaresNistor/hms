package com.hotelmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hotelmanagement.model.client.Room;

public interface RoomRepository extends JpaRepository<Room, String>{
	
	public List<Room> findByType(String type);
}
