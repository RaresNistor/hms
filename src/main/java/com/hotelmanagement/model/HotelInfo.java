package com.hotelmanagement.model;


import java.io.Serializable;

public class HotelInfo implements Serializable{
	
	private static final long serialVersionUID = 1993767120533974515L;

	private String name;
	
	private String latitude;
	
	private String longitude;
	
	private String address;
	
	private String description;
	
	private String phoneNumber;
	
	private String location;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLocation() {
		return latitude + "," + longitude;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
