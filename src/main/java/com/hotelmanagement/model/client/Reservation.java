package com.hotelmanagement.model.client;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "reservation")
public class Reservation {

	 @Id
	 @GeneratedValue(strategy = GenerationType.AUTO)
	 private Long id;
	 
	 @DateTimeFormat(pattern = "dd/MM/yyyy")
	 @Column(name = "start_date")
	 private Date startdate;
	 
	 @DateTimeFormat(pattern = "dd/MM/yyyy")
	 @Column(name = "end_date")
	 private Date endDate;
	 
	 @Column(name = "price")
	 private Double price;
	 
	 @Column(name = "payed")
	 private Boolean isPayed;
	 
	 @ManyToOne
	 @JoinColumn(name = "room_id")
	 private Room room;
	 
	 @ManyToOne
	 @JoinColumn(name = "client_id")
	 private Client client;
	 
	public Reservation(Long id, Date startdate, Date endDate, double price, Room room, Client client) {
		super();
		this.id = id;
		this.startdate = startdate;
		this.endDate = endDate;
		this.price = price;
		this.room = room;
		this.client = client;
	}

	public Reservation() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Boolean getIsPayed() {
		return isPayed;
	}

	public void setIsPayed(Boolean isPayed) {
		this.isPayed = isPayed;
	}
	 
}
