package com.hotelmanagement.model.dto;

import java.util.List;

public class RoomProduct {

	private String roomId;
	
	private Long productId;
	
	private List<Prod> products;

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public List<Prod> getProducts() {
		return products;
	}

	public void setProducts(List<Prod> products) {
		this.products = products;
	}
	
	
}
