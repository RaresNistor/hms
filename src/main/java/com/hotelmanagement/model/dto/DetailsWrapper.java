package com.hotelmanagement.model.dto;

import java.util.List;

public class DetailsWrapper {

	private List<Details> details;

	public List<Details> getDetails() {
		return details;
	}

	public void setDetails(List<Details> details) {
		this.details = details;
	}
	
	
}
