package com.hotelmanagement.serviceimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotelmanagement.model.client.Reservation;
import com.hotelmanagement.model.client.Room;
import com.hotelmanagement.model.dto.ReservationDTO;
import com.hotelmanagement.repository.ClientRepository;
import com.hotelmanagement.repository.ReservationRepository;
import com.hotelmanagement.repository.RoomRepository;
import com.hotelmanagement.service.MailService;
import com.hotelmanagement.service.ReservationService;

@Service("reservationService")
public class ReservationServiceImpl implements ReservationService{

	@Autowired
	ReservationRepository reservationRepository;

	@Autowired
	ClientRepository clientRepository;

	@Autowired
	RoomRepository roomRepository;

	@Autowired
	MailService mailService;

	@Override
	public void saveReservation(Reservation reservation, String user) {
		reservation.setClient(clientRepository.findByEmail(user).get());
		reservationRepository.save(reservation);
		try {
			mailService.send(reservation);
		} catch (MessagingException e) {
			//TODO
		}
	}

	@Override
	public Reservation map(ReservationDTO res) throws ParseException {
		return new Reservation(
				null,
				new SimpleDateFormat("dd/MM/yyyy").parse(res.getStartdate()), 
				new SimpleDateFormat("dd/MM/yyyy").parse(res.getEndDate()), 
				0, null, null);
	}

	@Override
	public List<Room> findReservations(Reservation res, String user, String roomType, int nrOfpersons) {
		List<Room> foundRooms = new ArrayList<>();
		List<Room> rooms = roomRepository.findByType(roomType);
		for(Room room : rooms) {
			if(0 == reservationRepository.checkIfRoomAvailable(res.getStartdate(), res.getEndDate(), room.getId()) 
					&& nrOfpersons <= room.getNumberOfPersons())
			{   
				room.setPrice(room.getPrice() * TimeUnit.DAYS.convert(res.getEndDate().getTime() - res.getStartdate().getTime(), TimeUnit.MILLISECONDS));
				foundRooms.add(room);
			}
		}
		return foundRooms;
	}

	@Override
	public List<Reservation> findReservationByClient(String clientId) {
		return clientRepository.findByEmail(clientId).get().getReservations();
	}

	@Override
	public boolean deleteReservation(long id) {
		boolean deleted = false;
		Optional<Reservation> res = reservationRepository.findById(id);
		Date date =  new Date();
		
		if(validateReservation(res, date))
		{
			reservationRepository.delete(res.get());
			deleted = true;
		}
		return deleted;
	}

	@Override
	public List<Reservation> findAll() {
		return reservationRepository.findAll();
	}

	private boolean validateReservation(Optional<Reservation> res, Date date) {
		return res.isPresent() && (res.get().getStartdate().after(date));
	}

	@Override
	public Reservation findReservationById(long id) {
		return reservationRepository.findById(id).get();
	}

	@Override
	public void saveReservation(Reservation res) {
		reservationRepository.save(res);
	}
	
	
}
