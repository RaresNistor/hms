package com.hotelmanagement.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotelmanagement.model.product.Suplier;
import com.hotelmanagement.repository.SuplierRepository;
import com.hotelmanagement.service.SupplierService;

@Service("suplierService")
public class SuplierServiceImpl implements SupplierService {

	@Autowired
	private SuplierRepository suplierRepository;

	@Override
	public List<Suplier> findAllSuppliers() {
		return suplierRepository.findAll();
	}

	@Override
	public Suplier findSuplierByName(String name) {
		return suplierRepository.findByName(name).get();
	}

	@Override
	public boolean save(Suplier supplier) {
		if (validateSupplier(supplier)) {
			suplierRepository.save(supplier);
			return true;
		}
		return false;
	}

	@Override
	public Suplier findSupplierByID(long id) {
		Optional<Suplier> suplier = suplierRepository.findById(id);
		if (suplier.isPresent()) {
			return suplier.get();
		}
		return null;
	}

	@Override
	public void deleteSupplier(long id) {
		suplierRepository.deleteById(id);
	}

	@Override
	public boolean update(Suplier supplier) {
		if (supplier.getId() != null && validateSupplier(supplier)) {
			suplierRepository.save(supplier);
			return true;
		}
		return false;
	}

	private boolean validateSupplier(Suplier supplier) {
		Optional<Suplier> sup;
		sup = suplierRepository.findByAddres(supplier.getAddres());
		boolean addressValid = !sup.isPresent() || (sup.isPresent() && sup.get().getId() == supplier.getId());
		sup = suplierRepository.findByName(supplier.getName());
		boolean nameValid = !sup.isPresent() || (sup.isPresent() && sup.get().getId() == supplier.getId());
		sup = suplierRepository.findByPhoneNumber(supplier.getPhoneNumber());
		boolean phoneValid = !sup.isPresent() || (sup.isPresent() && sup.get().getId() == supplier.getId());
		return addressValid && nameValid && phoneValid;
	}
}
