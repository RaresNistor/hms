package com.hotelmanagement.serviceimpl;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;

import com.hotelmanagement.model.HotelInfo;
import com.hotelmanagement.service.HotelInfoService;

@Service("hotelInfoService")
public class HotelInfoServiceImpl implements HotelInfoService{
	private static final  Logger LOGGER = Logger.getLogger(HotelInfoServiceImpl.class.getName());
	
	@Override
	public HotelInfo getHotelInfo() {
		HotelInfo hotelInfo = null;
		try (FileInputStream fileIn = new FileInputStream("hotel.ser"); 
			 ObjectInputStream in = new ObjectInputStream(fileIn)){
			hotelInfo = (HotelInfo) in.readObject();
		} catch (IOException|ClassNotFoundException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
		return hotelInfo;
	}

	@Override
	public void setHotelInfo(HotelInfo hotelInfo) {
		try (FileOutputStream fileOut =new FileOutputStream("hotel.ser")){
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(hotelInfo);
			out.close();
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
	}

}
