package com.hotelmanagement.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotelmanagement.model.client.Room;
import com.hotelmanagement.repository.RoomRepository;
import com.hotelmanagement.service.RoomService;

@Service("roomService")
public class RoomServiceImpl implements RoomService{

	@Autowired
	private RoomRepository roomRepository;
	
	@Override
	public void saveRoom(Room room) {
		roomRepository.save(room);
	}

	@Override
	public void deleteRoom(String id) {
		roomRepository.deleteById(id);
	}

	@Override
	public Room findRoomById(String id) {
		Optional<Room> room = roomRepository.findById(id);
		if(room.isPresent()) {
			return room.get();
		}
		return null;
	}

	@Override
	public List<Room> findAllRooms() {
		return  roomRepository.findAll();
	}

	@Override
	public void deepCopy(Room oldRoom, Room newRoom) {
		oldRoom.setNumberOfPersons(newRoom.getNumberOfPersons());
		oldRoom.setPrice(newRoom.getPrice());
		oldRoom.setType(newRoom.getType());
		oldRoom.setId(newRoom.getId());
	}

}
