package com.hotelmanagement.serviceimpl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.hotelmanagement.model.client.Reservation;
import com.hotelmanagement.service.MailService;

@Service("amilService")
public class MailServiceImpl implements MailService{

	@Autowired
	private JavaMailSender sender;

	@Override
	public void send(Reservation res) throws MessagingException {
		MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setTo(res.getClient().getEmail());
        String msg = "Hello, "+res.getClient().getFirstname() + " " +res.getClient().getLastname()
        		+"\n Your resrvation with the following information: \n"
        		+ "Stat date: " +res.getStartdate().toString()+"\n"
        		+ "End date: " + res.getEndDate().toString() + "\n"
        		+ "Room type: " + res.getRoom().getType()+ "\n"
        		+ "Price: " + res.getRoom().getPrice()+"\n"
        		+ "Has been Created. \n We will call you as soon as possible to set the further detayls!"; 
        helper.setText(msg);
        helper.setSubject("Your Reservation!");
        sender.send(message);

	}

}
