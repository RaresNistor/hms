package com.hotelmanagement.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.hotelmanagement.model.client.Client;
import com.hotelmanagement.model.user.Role;
import com.hotelmanagement.model.user.User;
import com.hotelmanagement.repository.ClientRepository;
import com.hotelmanagement.repository.RoleRespository;
import com.hotelmanagement.repository.UserRepository;
import com.hotelmanagement.service.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRespository roleRespository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private ClientRepository clientRepository;

	@Override
	public User findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public void saveUser(User user, boolean isClient) {
		if (isClient) {
			Client client = createClient(user);
			clientRepository.save(client);
			Role userRole = roleRespository.findByRole("ROLE_CLIENT");
			user.setRoles(new ArrayList<Role>(Arrays.asList(userRole)));
		}
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setActive(1);
		userRepository.save(user);
	}

	@Override
	public List<User> findAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public void deleteUser(Long id) {
		userRepository.deleteById(id);
	}

	@Override
	public void updateUser(User user) {
		userRepository.save(user);
	}

	private Client createClient(User user) {
		Client client = new Client();
		client.setEmail(user.getEmail());
		client.setFirstname(user.getFirstname());
		client.setLastname(user.getLastname());
		client.setPassword(user.getPassword());
		return client;
	}

	public List<Role> getAllRoles() {
		List<Role> roles = roleRespository.findAll();
		roles.removeIf(x -> x.getRole().equals("ROLE_CLIENT"));
		return roles;
	}

	@Override
	public void deepCopy(User oldUser, User newUser) {
		oldUser.setActive(1);
		oldUser.setEmail(newUser.getEmail());
		oldUser.setFirstname(newUser.getFirstname());
		oldUser.setLastname(newUser.getLastname());
		List<Role> roles = new ArrayList<>();
		roles.add(roleRespository.findByRole(newUser.getRrole()));
		oldUser.setRoles(roles);
	}

	@Override
	public void saveUser(User user) {
		userRepository.save(user);
	}

	@Override
	public User findUserById(long id) {
		if(!userRepository.findById(id).isPresent()) {
			return null;
		} else {
			return userRepository.findById(id).get();
		}
	}

}