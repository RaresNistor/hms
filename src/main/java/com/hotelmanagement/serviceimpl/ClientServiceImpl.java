package com.hotelmanagement.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotelmanagement.model.client.Client;
import com.hotelmanagement.model.user.Role;
import com.hotelmanagement.model.user.User;
import com.hotelmanagement.repository.ClientRepository;
import com.hotelmanagement.repository.RoleRespository;
import com.hotelmanagement.service.ClientService;

@Service("clientService")
public class ClientServiceImpl implements ClientService {

	@Autowired
	ClientRepository clientRepository;

	@Autowired
	RoleRespository roleRepository;

	@Override
	public Client findClientByEmail(String email) {
		Optional<Client> client = clientRepository.findByEmail(email);
		if(client.isPresent()) {
			return client.get();
		}
		return null;
	}

	@Override
	public void saveClient(Client client) {
		clientRepository.save(client);
	}

	@Override
	public User map(Client client) {
		User user = new User();
		user.setFirstname(client.getFirstname());
		user.setLastname(client.getLastname());
		user.setEmail(client.getEmail());
		List<Role> roles = new ArrayList<>();
		roles.add(roleRepository.findByRole("ROLE_CLIENT"));
		user.setRoles(roles);
		return user;
	}

	@Override
	public void deleteClient(Client client) {
		clientRepository.delete(client);
	}

	@Override
	public Client map(User user) {
		Client client = new Client();
		client.setFirstname(user.getFirstname());
		client.setLastname(user.getLastname());
		client.setEmail(user.getEmail());
		if(user.getPassword() != null) {
			client.setPassword(user.getPassword());
		}
		return client;
	}

	@Override
	public void map(Client oldClient, Client client) {
		oldClient.setFirstname(client.getFirstname());
		oldClient.setLastname(client.getLastname());
		oldClient.setPhoneNumber(client.getPhoneNumber());
		oldClient.setEmail(client.getEmail());
	}

	@Override
	public void map(User user, Client client) {
		user.setFirstname(client.getFirstname());
		user.setLastname(client.getLastname());
		user.setEmail(client.getEmail());
		List<Role> roles = new ArrayList<>();
		roles.add(roleRepository.findByRole("ROLE_CLIENT"));
		user.setRoles(roles);
	}

	@Override
	public Optional<Client> findClientById(long id) {
		return clientRepository.findById(id);
	}

	
}
