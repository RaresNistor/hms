package com.hotelmanagement.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotelmanagement.model.dto.Prod;
import com.hotelmanagement.model.product.Product;
import com.hotelmanagement.repository.ProductRepository;
import com.hotelmanagement.service.ProductService;

@Service("productService")
public class ProductServiceImpl implements ProductService{

	@Autowired
	ProductRepository productRepo;

	@Override
	public void deleteProduct(long id) {
		productRepo.deleteById(id);
	}

	@Override
	public Product getProduct(long id) {
		Optional<Product> product = productRepo.findById(id);
		if(product.isPresent()) {
			return  product.get();
		}
		return null;
	}

	@Override
	public void saveProduct(Product product) {
		productRepo.save(product);
	}

	@Override
	public List<Product> getAllProducts() {
		return productRepo.findAll();
	}

	@Override
	public List<Prod> getAllProds() {
		List<Prod> prods = new ArrayList<>();
		List<Product> products = new ArrayList<>();
		products.forEach( product -> prods.add(map(product)) );
		return prods;
	}

	private Prod map(Product product) {
		return new Prod(product.getName(), product.getId());
	}


}
