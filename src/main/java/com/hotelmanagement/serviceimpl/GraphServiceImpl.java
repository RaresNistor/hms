package com.hotelmanagement.serviceimpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotelmanagement.model.client.Reservation;
import com.hotelmanagement.service.GraphService;
import com.hotelmanagement.service.ReservationService;

@Service("graphService")
public class GraphServiceImpl implements GraphService{

	@Autowired
	ReservationService reservationService;

	@Override
	public List<Integer> getPiechart() {
		int single=0;
		int doublee=0;
		int triple=0;
		List<Reservation> all = reservationService.findAll();
		for(Reservation res: all) {
			switch (res.getRoom().getType()) {
			case "SINGLE":
				single++;
				break;
			case "DOUBLE":
				doublee++;
				break;
			case "TRIPLE":
				triple++;
				break;
			default:
				break;
			}
		}
		return new ArrayList<>(Arrays.asList(single, doublee, triple));
	}

	@Override
	public Map<Integer, Integer> getBarGraph() {
		Map<Integer, Integer> surveyMap = new LinkedHashMap<>();
		List<Reservation> all = reservationService.findAll();
		all.forEach( res -> createMap(surveyMap, res.getStartdate()));
		return surveyMap;
	}
	
	private void createMap(Map<Integer, Integer> map, Date date) {
		int year = getYear(date);
		if(map.containsKey(year)) {
			int value = map.get(year)+1;
			map.put(year, value);
		}else {
			map.put(year, 1);
		}
	}
	
	private int getYear(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.YEAR);
	}
}
